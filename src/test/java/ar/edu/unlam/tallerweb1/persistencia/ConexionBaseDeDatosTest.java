package ar.edu.unlam.tallerweb1.persistencia;

import ar.edu.unlam.tallerweb1.SpringTest;
import ar.edu.unlam.tallerweb1.modelo.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.*;

// Clase que prueba la conexion a la base de datos. Hereda de SpringTest por lo que corre dentro del contexto
// de spring
public class ConexionBaseDeDatosTest extends SpringTest{

    @Inject
    private SessionFactory sessionFactory;
    @Test
    @Transactional @Rollback
    public void pruebaConexion(){
        assertThat(session().isConnected()).isTrue();
    }

    @Test
    @Transactional @Rollback
    public void crearUsuario(){
        try {
            final Session session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        Usuario usuario = new Usuario();
        usuario.setEmail("seba@gmail.com");
        usuario.setPassword("1234");
        usuario.setRol("ADMIN");
        session().save(usuario);
        session().get(Usuario.class, usuario.getId());
    }

   @Test
   @Transactional @Rollback

    public void testFermacia(){
        Localidad localidad1 = new Localidad();
        Barrio barrio1 = new Barrio();
        Direccion direccion1 = new Direccion();

        localidad1.setNombre("Moron");
        barrio1.setNombre("moron");
        direccion1.setNumero(123);
        direccion1.setCalle("calle falsa");
        barrio1.setDireccion(direccion1);

       Farmacia farmacia1 = new Farmacia();
       farmacia1.setNombre("La morenita");
       farmacia1.setDireccion(direccion1);

   }
}
